var cordova = require('cordova'), Zip = require('./Zip');
module.exports = {
	unzip: function (win, fail, strInput) {
		console.log("src: " + strInput[0]);
		console.log("dest: " + strInput[1]);
		src = strInput[0];
		dest = strInput[1];

		var uriSrc = new Windows.Foundation.Uri(src);

		//la fonction getFolderFromApplicationUriAsync n'existe pas: on ne peut pas recuperer un storageFolder avec un uri de type 'ms-appdata:///'
		//on utilise donc directement le dossier de l'application (à changer si on veut changer de place...)
		dest = dest.replace(/ms-appdata:\/\/\/local\//, '');
		dest = dest.split('/');//(le repertoire TacFiles existe déjà, on le récupère et on crée notre dossier temp) (dest = [TacFiles, temp])
		
		var localStorage = Windows.Storage.ApplicationData.current.localFolder;
		//Windows.Storage.StorageFile.getFileFromPathAsync(src).done(function (zipFile) {
		Windows.Storage.StorageFile.getFileFromApplicationUriAsync(uriSrc).done(function (zipFile) {
			zipFile.copyAsync(localStorage, (zipFile.displayName).replace(/.tac/, '.zip')).done(function (FileTempExtZip) {
				localStorage.getFolderAsync(dest[0]).done(function (actFolder) {
					actFolder.createFolderAsync(dest[1]).done(function (zipFolder) {
						try {
							console.log("Unzipping file: " + zipFile.displayName + "...");
							ZipHelperWinRT.ZipHelper.unZipFileAsync(FileTempExtZip, zipFolder).done(function () {
								console.log(zipFile.displayName + ' unzipped successfully !');
								FileTempExtZip.deleteAsync().done(function () {
									zipFile.deleteAsync().done(function () {
										win('Unzip Process Success !');
									}, function (e) {
										fail(e);
									});
								}, function (e) {
									fail(e);
								});
							});
						}
						catch (err) {
							fail(err);
						}
					}, function (e) {
						fail(e);
					});
				}, function (e) {
					fail(e);
				});
			}, function (e) {
				fail(e);
			});
		}, function (e) {
			fail(e);
		});
	}
};
require("cordova/exec/proxy").add("Zip", module.exports);
//(C:\Users\{user}\AppData\Local\Packages\{app package}\LocalState\monZip.zip)
